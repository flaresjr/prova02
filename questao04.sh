#! /bin/bash

echo "CORES DE PROMPTS:"
echo "original 1"
echo "verde 2"
echo "azul 3"
echo "vermelho 4"



read -p "digite um numero:" op


test "$op" -eq "1" && export PS1="\[\e[0m\][\u@$\h]\[\e[32m\]\w\[\e[0m\]"
test "$op" -eq "2" && export PS1="\[\e[32m\][\u@$\h]\[\e[32m\]\w\[\e[0m\]"
test "$op" -eq "3" && export PS1="\[\e[34m\][\u@$\h]\[\e[32m\]\w\[\e[0m\]"
test "$op" -eq "4" && export PS1="\[\e[31m\][\u@$\h]\[\e[32m\]\w\[\e[0m\]"
